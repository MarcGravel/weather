import pytest, requests
from main import call_api, validate_location, print_response, validate_scale

#### Before testing, comment function get_input() call in main.py ####

def test_call_api():
    assert call_api("Chicago", "F") == 200

def test_call_api_not_found():
    assert call_api(00000, "C") == 400

# could not find how to test this without everything blowing up 
# def test_call_api_not_found():
#     assert call_api(00000) == type(requests.exceptions.RequestException)

@pytest.mark.parametrize('entry, expected',[
    ("Chicago", True),
    ("61761", True),
    ("123456", False),
    ("1234", False),
    ("12A34", False),
])
def test_validate_location(entry, expected):
    assert validate_location(entry) == expected

@pytest.mark.parametrize('entry, expected',[
    ("C", True),
    ("F", True),
    ("c", True),
    ("f", True),
    ("A", False),
    ("0", False),
])
def test_validate_scale(entry, expected):
    assert validate_scale(entry) == expected


def test_print_response():
    response_txt = "Today's weather in Chicago will be Partly cloudy with a temperature of 46.9F with NE winds blowing at 18.6 mph"
    assert print_response("Chicago", "Partly cloudy", "46.9F", "18.6 mph", "NE") == response_txt

