import requests

def get_input():
    user_input = input("Please enter a valid city name or zip code follow by a space and then F or C for your preferred scale: ")
    location, scale = user_input.split()
    if validate_location(location) and validate_scale(scale):
        call_api(location, scale)
    else:
        print("Please enter a valid city name or zip code follow by a space and then F or C for your preferred scale.")
        get_input()
def validate_location(location):
    if location.isnumeric():
        return len(location) == 5
    else:
        return location.isalpha()
    
def validate_scale(scale):
    return scale.isalpha() and (scale.lower() == "f" or scale.lower() == "c")

def call_api(location, scale):
    API_KEY = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    URL = "http://api.weatherapi.com/v1/current.json"
    parameters = {
        "key": API_KEY,
        "q": location,
        "aqi": "no",
    }
    try:
        response = requests.get(URL, params=parameters)
        if response.status_code == 200:
            data = response.json()
            description = data["current"]["condition"]["text"]
            if scale.lower() == 'f':
                temp = str(data["current"]["temp_f"]) + "F"
                wind_speed =  str(data["current"]["wind_mph"]) + " mph"
            else:
                temp = str(data["current"]["temp_c"]) + "C"
                wind_speed =  str(data["current"]["wind_kph"]) + " kph"
            wind_dir = data["current"]["wind_dir"]
            print_response(location, description, temp, wind_speed, wind_dir)
        
        elif response.status_code == 400:
            print("Entry not found.   Please enter a valid city name or zip code.")
            get_input()
        return response.status_code
    except requests.exceptions.RequestException:
        print(f"The API call was not succesfull: " + str(response.status_code))
        print(type(requests.exceptions.RequestException))
        return requests.exceptions.RequestException

def print_response(location, description, temp, wind_speed, wind_dir):
    forecast = "Today's weather in " + str(location) + " will be " + description + " with a temperature of " + str(temp) + " with " + str(wind_dir) + " winds blowing at " + str(wind_speed)
    print(forecast)
    return(forecast)
get_input()
